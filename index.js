//import thư viện express JS
const express = require ("express");

//Khai báo app 
const app = express ();

//khai báo cổng chạy app
const port = 8000;

//khai báo router
const randomNumberRouter = require("./app/routes/randomNumberRouter");
//app sử dụng middleware mỗi lần chạy
app.use("/", randomNumberRouter);

//app chạy trên cổng đã khai báo
app.listen(port, ()=>{
    console.log(`App is running on port ${port}`);
})